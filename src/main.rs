extern crate rand;

fn main() {
    println!("Hello, world!");
}

#[test]
fn adding_and_multiplying_works() {
    let x = rand::random::<u8>();
    let x = u16::from(x);
    println!("x = {}", x);
    assert_eq!(x + x, 2 * x);
}
